package com.example.fragmentgridlayout;

public class gridModel {
    int id;
    String sName;
    String sClass;


    public gridModel(int id, String sName, String sClass) {
        this.id = id;
        this.sName = sName;
        this.sClass = sClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsClass() {
        return sClass;
    }

    public void setsClass(String sClass) {
        this.sClass = sClass;
    }
}

