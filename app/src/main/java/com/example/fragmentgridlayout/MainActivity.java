package com.example.fragmentgridlayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<gridModel> gridholder;
    gridDataAdapter recyclerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.RV_SelectionSubjects);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        gridholder = new ArrayList<>();
        gridModel obj1 = new gridModel(1, "Raja", "Class 7");
        gridholder.add(obj1);
        gridModel obj2 = new gridModel(2, "Ramji", "Class 10");
        gridholder.add(obj2);
        gridModel obj3 = new gridModel(3, "Suriya", "CLass 4");
        gridholder.add(obj3);
        gridModel obj4 = new gridModel(4, "Seenu", "Class 3");
        gridholder.add(obj4);
        gridModel obj5 = new gridModel(5, "Rahul", "Class 6");
        gridholder.add(obj5);
        gridModel obj6 = new gridModel(6, "Ramki", "Class 5");
        gridholder.add(obj6);
        gridModel obj7 = new gridModel(7, "Dravid", "Class 12");
        gridholder.add(obj7);
        gridModel obj8 = new gridModel(8, "Selva", "Class 8");
        gridholder.add(obj8);
        gridModel obj9 = new gridModel(9, "Kannan", "Class 11");
        gridholder.add(obj9);
        gridModel obj10 = new gridModel(10, "Karthi", "Class 5");
        gridholder.add(obj10);
        recyclerAdapter = new gridDataAdapter(gridholder);
        recyclerView.setAdapter(recyclerAdapter);
    }

}