package com.example.fragmentgridlayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class gridDataAdapter extends RecyclerView.Adapter<gridDataAdapter.myviewholder> {
    ArrayList<gridModel> gridholder;


    public gridDataAdapter(ArrayList<gridModel> gridholder) {
        this.gridholder = gridholder;

    }


    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_grid, parent, false);
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position) {

        holder.sName.setText(gridholder.get(position).getsName());
        holder.sClass.setText(gridholder.get(position).getsClass());


    }

    @Override
    public int getItemCount() {
        return gridholder.size();
    }


    class myviewholder extends RecyclerView.ViewHolder {
        TextView sName, sClass;

        public myviewholder(@NonNull View itemView) {
            super(itemView);
            sName = itemView.findViewById(R.id.tv_SName);
            sClass = itemView.findViewById(R.id.tv_SClass);

        }
    }

}
